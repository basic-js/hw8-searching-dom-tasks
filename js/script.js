const paragraphs = document.querySelectorAll('p');

// for (let p of paragraphs) p.style.backgroundColor = '#ff0000';
paragraphs.forEach(p => p.style.backgroundColor = '#ff0000');

const optionsList = document.querySelector('#optionsList');

console.log(optionsList);
console.log(optionsList.parentNode);

for (let node of optionsList.childNodes) {
    console.log(node.nodeName); 
    console.log(node.nodeType); 
}

const testParagraph = document.querySelector('#testParagraph');

testParagraph.textContent = 'This is a paragraph';

const mainHeader = document.querySelector('.main-header');
const elemsMainHeader = mainHeader.children;

for (let elem of elemsMainHeader) {
    console.log(elem); 
    elem.classList.add('nav-item');
}

const elements = document.querySelectorAll('.section-title');

for (let element of elements) {
    element.classList.remove('section-title');
}