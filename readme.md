1. Опишіть своїми словами що таке Document Object Model (DOM)
   Це об'єктна модель документа, яка формується з HTML, CSS та JS.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
   innerText повертає тільки текст елемента, з урахуванням його стилів, а innerHTML повертає текст та теги внутрішніх елементів.
3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
   Краще використовувати document.querySelector або document.querySelectorAll.
   Можна також використовувати document.getElementById, getElementsByTagName, getElementsByClassName, getElementsByName.
